package com.malyshev;

import static java.lang.System.out;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.Entity;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Created by jiexa on 11.02.17.
 */
public class Deserializator {

  static final String outputEncoding = "UTF-8";

  private static PrintWriter out;
  private static int indent = 0;
  private static final String basicIndent = " ";

//    DOMEcho(PrintWriter out) {
//        this.out = out;
//    }

  private static void usage() {
    // ...
  }


  private final String filename;

  public Deserializator(String filename) {
    this.filename = filename;
  }

  public void deserialize() {


    Document doc = null;
    try {
      doc = creatingDOMDocument(filename);
      transformDocument(doc);
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (TransformerException e) {
      e.printStackTrace();
    }


  }

  private Document creatingDOMDocument(String filename)
      throws ParserConfigurationException, IOException, SAXException {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

    DocumentBuilder db = dbf.newDocumentBuilder();
    Document doc = db.parse(new File(filename));

    return doc;
  }

  private void transformDocument(Document doc) throws TransformerException {
    DOMSource domSource = new DOMSource(doc);

    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer xmlformer = transformerFactory.newTransformer();
    xmlformer.setOutputProperty(OutputKeys.INDENT, "yes");

    StringWriter sw = new StringWriter();
    StreamResult sr = new StreamResult(sw);
    xmlformer.transform(domSource, sr);

    System.out.println("\nDeserialized document:\n" + sw.toString() + "\n");
  }


  private static void printlnCommon(Node n) {
    out.print(" nodeName=\"" + n.getNodeName() + "\"");

    String val = n.getNamespaceURI();
    if (val != null) {
      out.print(" uri=\"" + val + "\"");
    }

    val = n.getPrefix();

    if (val != null) {
      out.print(" pre=\"" + val + "\"");
    }

    val = n.getLocalName();
    if (val != null) {
      out.print(" local=\"" + val + "\"");
    }

    val = n.getNodeValue();
    if (val != null) {
      out.print(" nodeValue=");
      if (val.trim().equals("")) {
        // Whitespace
        out.print("[WS]");
      } else {
        out.print("\"" + n.getNodeValue() + "\"");
      }
    }
    out.println();
  }

  private void outputIndentation() {
    for (int i = 0; i < indent; i++) {
      out.print(basicIndent);
    }
  }

  /**
   * Find the named subnode in a node's sublist.
   * <ul>
   * <li>Ignores comments and processing instructions.
   * <li>Ignores TEXT nodes (likely to exist and contain
   * ignorable whitespace, if not validating.
   * <li>Ignores CDATA nodes and EntityRef nodes.
   * <li>Examines element nodes to find one with
   * the specified name.
   * </ul>
   *
   * @param name the tag name for the element to find
   * @param node the element node to start searching from
   * @return the Node found
   */
  public Node findSubNode(String name, Node node) {
    if (node.getNodeType() != Node.ELEMENT_NODE) {
      System.err.println("Error: Search node not of element type");
      System.exit(22);
    }

    if (!node.hasChildNodes()) {
      return null;
    }

    NodeList list = node.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node subnode = list.item(i);
      if (subnode.getNodeType() == Node.ELEMENT_NODE) {
        if (subnode.getNodeName().equals(name)) {
          return subnode;
        }
      }
    }
    return null;
  }

  private void echo(Node n) {
    outputIndentation();
    int type = n.getNodeType();

    switch (type) {
      case Node.ATTRIBUTE_NODE:
        out.print("ATTR:");
        printlnCommon(n);
        break;

      case Node.CDATA_SECTION_NODE:
        out.print("CDATA:");
        printlnCommon(n);
        break;

      case Node.COMMENT_NODE:
        out.print("COMM:");
        printlnCommon(n);
        break;

      case Node.DOCUMENT_FRAGMENT_NODE:
        out.print("DOC_FRAG:");
        printlnCommon(n);
        break;

      case Node.DOCUMENT_NODE:
        out.print("DOC:");
        printlnCommon(n);
        break;

      case Node.DOCUMENT_TYPE_NODE:
        out.print("DOC_TYPE:");
        printlnCommon(n);
        NamedNodeMap nodeMap = ((DocumentType) n).getEntities();
        indent += 2;
        for (int i = 0; i < nodeMap.getLength(); i++) {
          Entity entity = (Entity) nodeMap.item(i);
          echo(entity);
        }
        indent -= 2;
        break;

      case Node.ELEMENT_NODE:
        out.print("ELEM:");
        printlnCommon(n);

        NamedNodeMap atts = n.getAttributes();
        indent += 2;
        for (int i = 0; i < atts.getLength(); i++) {
          Node att = atts.item(i);
          echo(att);
        }
        indent -= 2;
        break;

      case Node.ENTITY_NODE:
        out.print("ENT:");
        printlnCommon(n);
        break;

      case Node.ENTITY_REFERENCE_NODE:
        out.print("ENT_REF:");
        printlnCommon(n);
        break;

      case Node.NOTATION_NODE:
        out.print("NOTATION:");
        printlnCommon(n);
        break;

      case Node.PROCESSING_INSTRUCTION_NODE:
        out.print("PROC_INST:");
        printlnCommon(n);
        break;

      case Node.TEXT_NODE:
        out.print("TEXT:");
        printlnCommon(n);
        break;

      default:
        out.print("UNSUPPORTED NODE: " + type);
        printlnCommon(n);
        break;
    }

    indent++;
    for (Node child = n.getFirstChild(); child != null;
        child = child.getNextSibling()) {
      echo(child);
    }
    indent--;
  }


  private class MyErrorHandler implements ErrorHandler {

    private PrintWriter out;

    MyErrorHandler(PrintWriter out) {
      this.out = out;
    }

    private String getParseExceptionInfo(SAXParseException spe) {
      String systemId = spe.getSystemId();
      if (systemId == null) {
        systemId = "null";
      }

      String info = "URI=" + systemId + " Line=" + spe.getLineNumber() +
          ": " + spe.getMessage();
      return info;
    }

    public void warning(SAXParseException spe) throws SAXException {
      out.println("Warning: " + getParseExceptionInfo(spe));
    }

    public void error(SAXParseException spe) throws SAXException {
      String message = "Error: " + getParseExceptionInfo(spe);
      throw new SAXException(message);
    }

    public void fatalError(SAXParseException spe) throws SAXException {
      String message = "Fatal Error: " + getParseExceptionInfo(spe);
      throw new SAXException(message);
    }
  }
}
