package com.malyshev;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Hashtable;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by jiexa on 15.02.17.
 */
public class JarClassLoader extends ClassLoader {
  private String jarFileURL;
  private String jarFileOS;
  private Hashtable classes = new Hashtable();


  public void setJarPaths(String jarFileURL, String jarFileOS) {
    this.jarFileURL = jarFileURL;
    this.jarFileOS = jarFileOS;
  }

  public JarClassLoader() {
    super(JarClassLoader.class.getClassLoader());
  }

  public Class loadClass(String className) throws ClassNotFoundException {
    return findClass(className);
  }

  @Override
  public Class findClass(String className) {
    byte classByte[];
    Class result = null;

    result = (Class) classes.get(className);
    if (result != null) {
      return result;
    }

    try {
      return findSystemClass(className);
    } catch (Exception e) {
    }

    try {
      downloadJar();
      JarFile jar = new JarFile(jarFileOS);
      JarEntry entry = jar.getJarEntry(className + ".class");
      InputStream is = jar.getInputStream(entry);
      ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
      int nextValue = is.read();
      while (-1 != nextValue) {
        byteStream.write(nextValue);
        nextValue = is.read();
      }

      classByte = byteStream.toByteArray();
      result = defineClass(className, classByte, 0, classByte.length, null);
      classes.put(className, result);
      return result;
    } catch (Exception e) {
      return null;
    }
  }

  private void downloadJar() throws IOException {
    URL url = new URL(jarFileURL);

    InputStream inStream = url.openStream();
    BufferedInputStream bufIn = new BufferedInputStream(inStream);

    File fileWrite = new File(jarFileOS);
    OutputStream out= new FileOutputStream(fileWrite);

    BufferedOutputStream bufOut = new BufferedOutputStream(out);
    byte data[] = new byte[1024];
    int count;
    while( (count = bufIn.read(data,0,1024)) != -1){
      bufOut.write(data,0,count);
    }

    bufOut.flush();
    out.close();
    inStream.close();
  }

}
