package com.malyshev;

/**
 * Created by jiexa on 10.02.17.
 */
public class People {
    private String name;
    private Integer age;
    private  Double sallary;

    public People(String name, Integer age, Double sallary) {
        this.name = name;
        this.age = age;
        this.sallary = sallary;
    }

    public People() {
        super();
        this.name = "John";
        this.age = 16;
        this.sallary = 30000.0;
    }

    protected void  paySallary() {
        System.out.println("I have sallary " + sallary);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getSallary() {
        return sallary;
    }

    public void setSallary(Double sallary) {
        this.sallary = sallary;
    }
}
