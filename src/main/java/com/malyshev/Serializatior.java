package com.malyshev;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.lang.reflect.Field;

/**
 * Created by jiexa on 10.02.17.
 */
public class Serializatior {
    private Document doc;

    public void serialize(Object obj) {
        try {
            doc = createDocument();
            fillDocument(obj);
            saveDocument();
            System.out.println("Документ успешно загружен");
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    private Document createDocument () throws ParserConfigurationException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation impl = builder.getDOMImplementation();

        return impl.createDocument(null, null, null);
    }

    private void fillDocument (Object obj) throws IllegalAccessException {

        Element e1 = doc.createElement("object");
        doc.appendChild(e1);
        e1.setAttribute("type", obj.getClass().getTypeName());
        for (int i = 0; i < obj.getClass().getDeclaredFields().length; i++) {

            Field field = obj.getClass().getDeclaredFields()[i];
            field.setAccessible(true);

            Element eChild = doc.createElement("field");
            e1.appendChild(eChild);

            String id = field.getName();
            eChild.setAttribute("id", id);

            String type = field.getType().getTypeName();
            eChild.setAttribute("type", type);

            String value = field.get(obj).getClass().toString();
                eChild.setAttribute("value", value);
        }
    }

    private void saveDocument() throws TransformerException {

        DOMSource domSource = new DOMSource(doc);

        TransformerFactory transformerFactory=TransformerFactory.newInstance();
        Transformer xmlformer=transformerFactory.newTransformer();
        xmlformer.setOutputProperty(OutputKeys.INDENT,"yes");

        File peopleXML = new File("animal.xml");
        StreamResult result=new StreamResult(peopleXML);

        xmlformer.transform(domSource,result);

    }
}
