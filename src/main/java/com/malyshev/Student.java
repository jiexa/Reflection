package com.malyshev;

/**
 * Created by jiexa on 11.02.17.
 */
public class Student {

  private String name;
  private Integer age;
  private Double scholarship;

  public Student(String name, Integer age, Double scholarship) {
    this.name = name;
    this.age = age;
    this.scholarship = scholarship;
  }

  public Student() {

  }

  protected void  showScholarship() {
    System.out.println("I have scholarship " + scholarship + " руб.");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public Double getScholarship() {
    return scholarship;
  }

  public void setScholarship(Double scholarship) {
    this.scholarship = scholarship;
  }
}
