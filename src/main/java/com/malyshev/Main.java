package com.malyshev;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

public class Main {

  public static void main(String[] args)
      throws ParserConfigurationException, IOException, SAXException, TransformerException, NoSuchFieldException, ClassNotFoundException, IllegalAccessException, InstantiationException {

    JarClassLoader jarClassLoader = new JarClassLoader();
    jarClassLoader.setJarPaths("https://gitlab.com/jiexa/test/raw/master/Reflection-1.0.jar", "file.jar");

    Class<?> clazz = jarClassLoader.loadClass("com.malyshev.People");

    Serializatior serializatior = new Serializatior();

    serializatior.serialize(clazz.newInstance());

      Deserializator deserializator = new Deserializator("student.xml");

      deserializator.deserialize();

  }

  public static void downloadFiles(String urlString, String jarFile) throws IOException {

    BufferedInputStream in = null;
    FileOutputStream fout = null;

    in = new BufferedInputStream(new URL(urlString).openStream());
    fout = new FileOutputStream(jarFile);

    int count = in.available();
    final byte data[] = new byte[count];
    in.read(data);
    fout.write(data);

    in.close();
    fout.close();
  }

}


